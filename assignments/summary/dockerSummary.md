#                                                      **Docker** 
![](extras/DC.png)

# **About Docker-**
Docker is a tool designed to make it easier to create, deploy, and run applications by providing a complete environment to build and deploy software.
Docker uses the concept of containers, which allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package. By doing so, it can be run on any machine regardless of its configurations and versions.
So, it is a great tool for continuous deployment of product with efficient use of resources available.

# **Architecture-**

The components of a docker architecture-

**1. Docker Engine**
It is the core part of the whole Docker system. It is installed on the host machine. There are three components in the Docker Engine:
•	Server
•	Rest API: 
•	Command Line Interface (CLI)

**2. Docker Client**
Docker users can interact with Docker through a client. Docker client can communicate with more than one daemon.

**3. Docker Registries**
It is the location where the Docker images are stored. You can also create and run your own private registry.

**4. Docker Objects**
When you are working with Docker, you use images, containers, volumes, networks; all these are Docker objects.
 ![](extras/AR.png)

 


# **Avantages and Disadvantages**
![](extras/analyzing-data-with-docker-v4-20-638.png)

# **Applications of Docker**
![](extras/dcapp.png)




