# **GitLab Workflow**
Git is a version control system which enables you to track changes to files. It is entirely file based itself, meaning there is no additioanl software or applications required except Git istelf.
Using Git, you are able to revert files back to previous versions, restore deleted files, remove added files and even track down where a particular line of code was introduced.
Git creates a .git folder (in the current folder) to store the details of the file system - this folder contains all the data required to track your files and is known as a repository, or repo.
Git tracks file changes by the user creating a save point, or in Git terms a commit. Each commit takes a snapshot of the current file system rather than storing just the changes made since the last commit.

## Some basic Git commands are attached:

![](extras/1.png)


**Also, the basic workflow diagram are also mentioned below:**

![](extras/2.png)


**Now a workflow should be simple and enhance the productivity of the team. There are various workflows to understand the concept:**

## •	centralized workflow:

![](extras/3.png)

## •	forking workflow:

![](extras/4.png)


## •	Branching workflow:

![](extras/5.png)


![](extras/6.png)


## •	Gitflow workflow:

![](extras/7.png)


## So, the basic Git workflow is:

![](extras/8.png)





